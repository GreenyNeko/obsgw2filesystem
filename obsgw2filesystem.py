import requests, json # to receive data from the api
import configparser # to parse the .ini
import time # to add a delay
import sys, traceback # for error handling and throwing

# global variables
debugOutput = ''
floatAccuracy = 9 # default is 9
# the initial value on the start of the program
initialValue = dict()
errorCount = 0

class MaxErrorsReachedException(Exception):
    def __init__(self, message, arg2=None):
        self.arg1 = message
        self.arg2 = arg2
        super().__init__(message)

# helper function
def printAndLog(output):
    global debugOutput
    debugOutput    += output + '\n'
    print(output)
    
def writeToFileAndLog(filePath, input):
    printAndLog('[LOG] Writing to ' + filePath)
    with open(filePath, 'w') as f:
        f.write(input)
    f.close()
    
def initAndStoreData(category, path, statTag, stat):
    global floatAccuracy
    if statTag not in initialValue:
        initialValue[statTag] = stat
    if category['current-' + statTag]:
        if type(stat) is float:
            writeToFileAndLog(filePath + path + statTagToCamelCase('current-' + statTag) + '.txt', str(round(stat - initialValue[statTag], floatAccuracy)))
        else:
            writeToFileAndLog(filePath + path + statTagToCamelCase('current-' + statTag) + '.txt', str(stat - initialValue[statTag]))
    if category['total-' + statTag]:
        if type(stat) is float:
            writeToFileAndLog(filePath + path + statTagToCamelCase('total-' + statTag) + '.txt', str(round(stat, floatAccuracy)))
        else:
            writeToFileAndLog(filePath + path + statTagToCamelCase('total-' + statTag) + '.txt', str(stat))

def storeData(category, path, statTag, stat):
    if category[statTag]:
        if type(stat) is float:
            writeToFileAndLog(filePath + path + statTagToCamelCase(statTag) + '.txt', str(round(stat, floatAccuracy)))
        else:
            writeToFileAndLog(filePath + path + statTagToCamelCase(statTag) + '.txt', str(stat))
        
        
def statTagToCamelCase(statTag):
    parts = statTag.split('-')
    for i in range(1, len(parts)):
        parts[i] = parts[i][0].upper() + parts[i][1:]
    return "".join(parts)
    
def getIdsOfMaps(dataScope):
    global errorCount
    mapIds = dict()
    for i in range(4):
        if dataScope[i]['type'] == 'RedHome':
            mapIds['red'] = i
        elif dataScope[i]['type'] == 'BlueHome':
            mapIds['blue'] = i
        elif dataScope[i]['type'] == 'GreenHome':
            mapIds['green'] = i
        elif dataScope[i]['type'] == 'Center':
            mapIds['eternal'] = i
        else:
            errorCount += 1
            printAndLog('[ERROR] Unable to assign id to map \'' + dataScope[i]['type'] + '.')
    return mapIds
    
def handleAPIMsg(dataMessage):
    global errorCount
    # ah oh... api wants to tell us something
    if dataMessage['text'] == 'invalid key':
        errorCount += 1
        printAndLog('[ERROR] Invalid key. The way obsgw2filesystem is set up, requires an api-key. Check your API-key and .ini file')
    if dataMessage['text'] == 'world not currently in a match':
        errorCount += 1
        printAndLog('[ERROR] World not currently in a match. Check the correctness of the world id.')

printAndLog('Starting program...')
try:
    # read config file
    printAndLog('[LOG] Loading config...')
    config = configparser.ConfigParser()
    config.read('obsgw2filesystem.ini')
    pollFrequency = config.get('General', 'poll-frequency')
    filePath = config.get('General', 'filepath')
    hideApiKey = config.getboolean('General', 'hide-api-key')
    floatAccuracy = int(config.get('General', 'floating-point-accuracy'))
    allowedErrors = config.get('General', 'allowed-max-errors')    
    wvwServerId = config.get('Account', 'wvw-server-id')
    apiKey = config.get('Account', 'api-key')
    printAndLog('[LOG] Polls every ' + pollFrequency + ' seconds.')
    printAndLog('[LOG] Saving files to ' + filePath)
    printAndLog('[LOG] Allowed errors per poll before stopping: ' + allowedErrors + '.')
    printAndLog('[LOG] Selected WvW Server id: ' + wvwServerId)
    if not hideApiKey:
        printAndLog('[LOG] Using following api-key: ' + apiKey)
    else:
        printAndLog('[LOG] Api-Key is hidden.')

    enabledCategories = dict()
    options = config.options('Categories')
    for option in options:
        try:
            enabledCategories[option] = config.getboolean('Categories', option)
        except:
            printAndLog('[WARN] Unable to load key \'' + option + '\' from .ini.')
            enabledCategories[option] = False
    
    # get configuration for misc values if enabled
    if enabledCategories['misc']:
        miscValues = dict()
        options = config.options('Misc')
        for option in options:
            try:
                miscValues[option] = config.getboolean('Misc', option)
            except:
                printAndLog('[WARN] Unable to load key \'' + option + '\' from .ini.')
                miscValues[option] = False
    
    if enabledCategories['wvw']:
        wvwValues = dict()
        options = config.options('WvW')
        for option in options:
            try:
                wvwValues[option] = config.getboolean('WvW', option)
            except:
                printAndLog('[WARN] Unable to load key \'' + option + '\' from .ini.')
                wvwValues[option] = False
                
    if enabledCategories['achievements']:
        achievementValues = dict()
        options = config.options('Achievements')
        for option in options:
            try:
                achievementValues[option] = config.getboolean('Achievements', option)
            except:
                printAndLog('[WARN] Unable to load key \'' + option + '\' from .ini.')
                achievementValues[option] = False

    #get a requests per minute
    printAndLog('[LOG] Asking for request rate limit...')
    response = requests.get("https://api.guildwars2.com/v2/quaggans") # request for header
    requestsPerMinute = 600
    try:
        requestsPerMinute = int(response.headers['X-Rate-Limit-Limit']) - 1 #599 requests left :P
        printAndLog('[LOG] API allows ' + str(requestsPerMinute + 1) + ' requests per minute.')
    except KeyError:
        printAndLog('[WARN] ArenaNet didn\'t send the limit. Assuming 600.')
    passedTime = 0
    requestQuota = requestsPerMinute
    while True:
        if errorCount > int(allowedErrors):
            raise MaxErrorsReachedException('The specified maximum of errors was reached!', errorCount)
        try:
            startTime = time.time()
            # achievements
            if enabledCategories['achievements']:
                printAndLog('[LOG] Sending requests for achievements')
                if requestQuota > 0:
                    try:
                        response = requests.get('https://api.guildwars2.com/v2/account/achievements?access_token=' + apiKey)
                        data = json.loads(response.text)
                        requestQuota -= 1
                    except ValueError:
                        printAndLog('[WARN] Received an invalid response from API. Skipping until next poll.')
                        continue
                    if 'text' in data:
                        handleAPIMsg(data)
                        continue
                    kills = 0
                    for i in range(0, len(data)):
                        if data[i]['id'] == 283:
                            kills = data[i]['current']
                    initAndStoreData(achievementValues, '/achievements/', 'wvw-kills', kills) 
                else:
                    printAndLog('[LOG] Ran out of request quota.. abort.')
            # misc category
            if enabledCategories['misc']:
                printAndLog('[LOG] Sending requests for misc...')
                #get the amount of characters
                if requestQuota > 0:
                    try:
                        response = requests.get('https://api.guildwars2.com/v2/characters?page=0&access_token=' + apiKey)
                        requestQuota -= 1
                        characters = json.loads(response.text) # update amount of characters
                    except ValueError:
                        printAndLog('[WARN] Received an invalid response from API. Skipping until next poll.')
                        continue
                    if 'text' in characters:
                        handleAPIMsg(characters)
                        continue
                    deathCount = 0
                    for character in characters:
                        deathCount += int(character['deaths'])
                    initAndStoreData(miscValues, '/misc/', 'player-deaths', deathCount) 
                else:
                    printAndLog('[LOG] Ran out of request quota.. abort.')
            # wvw category
            if enabledCategories['wvw']:
                printAndLog('[LOG] Sending requests for wvw...')
                if requestQuota > 0:
                    try:
                        response = requests.get('https://api.guildwars2.com/v2/wvw/matches?world=' + wvwServerId)
                        requestQuota -= 1
                        data = json.loads(response.text)
                    except ValueError:
                        printAndLog('[WARN] Received an invalid response from API. Skipping until next poll.')
                        continue
                    if 'text' in data:
                        handleAPIMsg(data)
                        continue
                    #save in respective file
                    printAndLog('[LOG] Saving data to files...')
                    initAndStoreData(wvwValues, '/wvw/', 'kills-green', data['kills']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'kills-red', data['kills']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'kills-blue', data['kills']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'deaths-green', data['deaths']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'deaths-red', data['deaths']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'deaths-blue', data['deaths']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'victory-points-red', data['victory_points']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'victory-points-blue', data['victory_points']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'victory-points-green', data['victory_points']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'score-green', data['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'score-red', data['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'score-blue', data['scores']['blue'])
                    # get the map ids from the maps
                    mapIds = getIdsOfMaps(data['maps'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-score-green', data['maps'][mapIds['green']]['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-score-red', data['maps'][mapIds['green']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-score-blue', data['maps'][mapIds['green']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-score-green', data['maps'][mapIds['blue']]['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-score-red', data['maps'][mapIds['blue']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-score-blue', data['maps'][mapIds['blue']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-score-green', data['maps'][mapIds['red']]['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-score-red', data['maps'][mapIds['red']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-score-blue', data['maps'][mapIds['red']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-score-blue', data['maps'][mapIds['eternal']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-score-red', data['maps'][mapIds['eternal']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-score-green', data['maps'][mapIds['eternal']]['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-kills-red', data['maps'][mapIds['green']]['kills']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-kills-green', data['maps'][mapIds['green']]['kills']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-kills-blue', data['maps'][mapIds['green']]['kills']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-kills-green', data['maps'][mapIds['blue']]['kills']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-kills-blue', data['maps'][mapIds['blue']]['kills']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-kills-red', data['maps'][mapIds['blue']]['kills']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-kills-green', data['maps'][mapIds['red']]['kills']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-kills-blue', data['maps'][mapIds['red']]['kills']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-kills-red', data['maps'][mapIds['blue']]['kills']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-kills-blue', data['maps'][mapIds['eternal']]['kills']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-kills-green', data['maps'][mapIds['eternal']]['kills']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-kills-red', data['maps'][mapIds['eternal']]['kills']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-deaths-green', data['maps'][mapIds['green']]['deaths']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-deaths-red', data['maps'][mapIds['green']]['deaths']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'green-map-deaths-blue', data['maps'][mapIds['green']]['deaths']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-deaths-green', data['maps'][mapIds['red']]['deaths']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-deaths-red', data['maps'][mapIds['red']]['deaths']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'red-map-deaths-blue', data['maps'][mapIds['red']]['deaths']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-deaths-green', data['maps'][mapIds['blue']]['deaths']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-deaths-blue', data['maps'][mapIds['blue']]['deaths']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'blue-map-deaths-red', data['maps'][mapIds['blue']]['deaths']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-deaths-green', data['maps'][mapIds['eternal']]['deaths']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-deaths-red', data['maps'][mapIds['eternal']]['deaths']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'eternal-map-deaths-blue', data['maps'][mapIds['eternal']]['deaths']['blue'])
                    # kd green
                    kills = (float(data['kills']['green']))
                    deaths = (float(data['deaths']['green']))
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-kd-green', kills / deaths)
                    kills = (float(data['kills']['green'])) - (float(initialValue['kills-green']))
                    deaths = (float(data['deaths']['green'])) - (float(initialValue['deaths-green']))
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-kd-green', kills / deaths)
                    # kd blue
                    kills = (float(data['kills']['blue']))
                    deaths = (float(data['deaths']['blue']))
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-kd-blue', kills / deaths)
                    kills = (float(data['kills']['blue'])) - (float(initialValue['kills-blue']))
                    deaths = (float(data['deaths']['blue'])) - (float(initialValue['deaths-blue']))
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-kd-blue', kills / deaths)
                    # kd red
                    kills = (float(data['kills']['red']))
                    deaths = (float(data['deaths']['red']))
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-kd-red', kills / deaths)
                    kills = (float(data['kills']['red'])) - (float(initialValue['kills-red']))
                    deaths = (float(data['deaths']['red'])) - (float(initialValue['deaths-red']))
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-kd-red', kills / deaths)
                    # green map kd red
                    kills = int(data['maps'][mapIds['green']]['kills']['red'])
                    deaths = int(data['maps'][mapIds['green']]['deaths']['red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-green-map-kd-red', kills / deaths)
                    kills = int(data['maps'][mapIds['green']]['kills']['red']) - int(initialValue['green-map-kills-red'])
                    deaths = int(data['maps'][mapIds['green']]['deaths']['red']) - int(initialValue['green-map-deaths-red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-green-map-kd-red', kills / deaths)
                    # green map kd blue
                    kills = int(data['maps'][mapIds['green']]['kills']['blue'])
                    deaths = int(data['maps'][mapIds['green']]['deaths']['blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-green-map-kd-blue', kills / deaths)
                    kills = int(data['maps'][mapIds['green']]['kills']['blue']) - int(initialValue['green-map-kills-blue'])
                    deaths = int(data['maps'][mapIds['green']]['deaths']['blue']) - int(initialValue['green-map-deaths-blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-green-map-kd-blue', kills / deaths)
                    # green map kd green
                    kills = int(data['maps'][mapIds['green']]['kills']['green'])
                    deaths = int(data['maps'][mapIds['green']]['deaths']['green'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-green-map-kd-green', kills / deaths)
                    kills = int(data['maps'][mapIds['green']]['kills']['green']) - int(initialValue['green-map-kills-green'])
                    deaths = int(data['maps'][mapIds['green']]['deaths']['green']) - int(initialValue['green-map-deaths-green'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-green-map-kd-green', kills / deaths)
                    # blue map kd red
                    kills = int(data['maps'][mapIds['blue']]['kills']['red'])
                    deaths = int(data['maps'][mapIds['blue']]['deaths']['red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-blue-map-kd-red', kills / deaths)
                    kills = int(data['maps'][mapIds['blue']]['kills']['red']) - int(initialValue['blue-map-kills-red'])
                    deaths = int(data['maps'][mapIds['blue']]['deaths']['red']) - int(initialValue['blue-map-deaths-red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-blue-map-kd-red', kills / deaths)
                    # blue map kd blue
                    kills = int(data['maps'][mapIds['blue']]['kills']['blue'])
                    deaths = int(data['maps'][mapIds['blue']]['deaths']['blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-blue-map-kd-blue', kills / deaths)
                    kills = int(data['maps'][mapIds['blue']]['kills']['blue']) - int(initialValue['blue-map-kills-blue'])
                    deaths = int(data['maps'][mapIds['blue']]['deaths']['blue']) - int(initialValue['blue-map-deaths-blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-blue-map-kd-blue', kills / deaths)
                    # blue map kd green
                    kills = int(data['maps'][mapIds['blue']]['kills']['green'])
                    deaths = int(data['maps'][mapIds['blue']]['deaths']['green'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-blue-map-kd-green', kills / deaths)
                    kills = int(data['maps'][mapIds['blue']]['kills']['green']) - int(initialValue['blue-map-kills-green'])
                    deaths = int(data['maps'][mapIds['blue']]['deaths']['green']) - int(initialValue['blue-map-deaths-green'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-blue-map-kd-green', kills / deaths)
                    # eternal map kd blue
                    kills = int(data['maps'][mapIds['eternal']]['kills']['blue'])
                    deaths = int(data['maps'][mapIds['eternal']]['deaths']['blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-eternal-map-kd-blue', kills / deaths)
                    kills = int(data['maps'][mapIds['eternal']]['kills']['blue']) - int(initialValue['eternal-map-kills-blue'])
                    deaths = int(data['maps'][mapIds['eternal']]['deaths']['blue']) - int(initialValue['eternal-map-deaths-blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-eternal-map-kd-blue', kills / deaths)
                    # eternal map kd red
                    kills = int(data['maps'][mapIds['eternal']]['kills']['red'])
                    deaths = int(data['maps'][mapIds['eternal']]['deaths']['red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-eternal-map-kd-red', kills / deaths)
                    kills = int(data['maps'][mapIds['eternal']]['kills']['red']) - int(initialValue['eternal-map-kills-red'])
                    deaths = int(data['maps'][mapIds['eternal']]['deaths']['red']) - int(initialValue['eternal-map-deaths-red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-eternal-map-kd-red', kills / deaths)
                    # eternal map kd green
                    kills = int(data['maps'][mapIds['eternal']]['kills']['green'])
                    deaths = int(data['maps'][mapIds['eternal']]['deaths']['green'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-eternal-map-kd-green', kills / deaths)
                    kills = int(data['maps'][mapIds['eternal']]['kills']['green']) - int(initialValue['eternal-map-kills-green'])
                    deaths = int(data['maps'][mapIds['eternal']]['deaths']['green']) - int(initialValue['eternal-map-deaths-green'])
                    printAndLog("[DEBUG] kills: " + str(kills))
                    printAndLog("[DEBUG] deaths: " + str(deaths))
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-eternal-map-kd-green', kills / deaths)
                    # red map kd blue
                    kills = int(data['maps'][mapIds['red']]['kills']['blue'])
                    deaths = int(data['maps'][mapIds['red']]['deaths']['blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-red-map-kd-blue', kills / deaths)
                    kills = int(data['maps'][mapIds['red']]['kills']['blue']) - int(initialValue['red-map-kills-blue'])
                    deaths = int(data['maps'][mapIds['red']]['deaths']['blue']) - int(initialValue['red-map-deaths-blue'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-red-map-kd-blue', kills / deaths)
                    # red map kd red
                    kills = int(data['maps'][mapIds['red']]['kills']['red'])
                    deaths = int(data['maps'][mapIds['red']]['deaths']['red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-red-map-kd-red', kills / deaths)
                    kills = int(data['maps'][mapIds['red']]['kills']['red']) - int(initialValue['red-map-kills-red'])
                    deaths = int(data['maps'][mapIds['red']]['deaths']['red']) - int(initialValue['red-map-deaths-red'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-red-map-kd-red', kills / deaths)
                    # red map kd green
                    kills = int(data['maps'][mapIds['red']]['kills']['green'])
                    deaths = int(data['maps'][mapIds['red']]['deaths']['green'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'total-red-map-kd-green', kills / deaths)
                    kills = int(data['maps'][mapIds['red']]['kills']['green']) - int(initialValue['red-map-kills-green'])
                    deaths = int(data['maps'][mapIds['red']]['deaths']['green']) - int(initialValue['red-map-deaths-green'])
                    if deaths == 0:
                        deaths = 1
                    storeData(wvwValues, '/wvw/', 'current-red-map-kd-green', kills / deaths)
                    # shorten the last skirmish
                    dataLastSkirmish = data['skirmishes'][-1:][0]
                    # get the map ids from the last skirmish
                    mapIds = getIdsOfMaps(dataLastSkirmish['map_scores'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-score-red', dataLastSkirmish['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-score-blue', dataLastSkirmish['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-score-green', dataLastSkirmish['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-green-map-score-red', dataLastSkirmish['map_scores'][mapIds['green']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-green-map-score-green', dataLastSkirmish['map_scores'][mapIds['green']]['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-green-map-score-blue', dataLastSkirmish['map_scores'][mapIds['green']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-blue-map-score-red', dataLastSkirmish['map_scores'][mapIds['blue']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-blue-map-score-green', dataLastSkirmish['map_scores'][mapIds['blue']]['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-blue-map-score-blue', dataLastSkirmish['map_scores'][mapIds['blue']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-eternal-map-score-red', dataLastSkirmish['map_scores'][mapIds['eternal']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-eternal-map-score-blue', dataLastSkirmish['map_scores'][mapIds['eternal']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-eternal-map-score-green', dataLastSkirmish['map_scores'][mapIds['eternal']]['scores']['green'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-red-map-score-red', dataLastSkirmish['map_scores'][mapIds['red']]['scores']['red'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-red-map-score-blue', dataLastSkirmish['map_scores'][mapIds['red']]['scores']['blue'])
                    initAndStoreData(wvwValues, '/wvw/', 'recent-red-map-score-green', dataLastSkirmish['map_scores'][mapIds['red']]['scores']['green'])
                else:
                    printAndLog('[LOG] Ran out of request quota.. abort.')
            if enabledCategories['pvp']:
                printAndLog('[LOG] Sending requests for PvP...')
                if requestQuota > 0:
                    try:
                        response = requests.get('https://api.guildwars2.com/v2/pvp/seasons?ids=all')
                        requestQuota -= 1
                        dataSeasons = json.loads(response.text)
                    except ValueError:
                        printAndLog('[WARN] Received an invalid response from API. Skipping until next poll.')
                        continue
                    if 'text' in data:
                        handleAPIMsg(dataSeasons)
                        continue
                    #save in respective file
                    for season in dataSeasons:
                        pass # there is no season 15!?!?!?
                    printAndLog('[LOG] Saving data to files...')
            printAndLog('[LOG] Waiting...')
            time.sleep(int(pollFrequency))
            errorCount = 0
            passedTime += time.time() - startTime
            if passedTime > 60: # minute passed
                printAndLog('[LOG] Request quota has been refreshed')
                requestQuota = requestsPerMinute # refill our quota
        except Exception:
            printAndLog('[ERROR] An error occured:')
            printAndLog('\t' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            printAndLog('\t' + str(traceback.format_tb(sys.exc_info()[2], 10)))
            errorCount += 1
            continue
except MaxErrorsReachedException:
    printAndLog('[ERROR] Too many errors occured:')
    printAndLog('\t' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
    printAndLog('\t' + str(traceback.format_tb(sys.exc_info()[2], 10)))
    debugFile = open('error.log', 'w')
    debugFile.write(debugOutput)
    debugFile.close()

