# OBSGw2FileSystem

Python program to download the data from the Guild Wars 2 API and save it into text files which can be used for  Open Broadcast Software.

Make sure to check out the .ini file and set it up accordingly, so you do not necessarily download things you don't want which at times can take it's time.
Especially if you have multiple characters.

# File Structure

The created filesystem requires an folder structure in the specified folder that looks like this:

wvw/
misc/
achievements/

So with the default 'files' the complete structure will look like this:

obsgw2filesystem.py
obsgw2filesystem.ini
files/wvw/
files/misc/
files/achievements

# Integrating into OBS

To add the data to your OBS create a new 'Text (GDI+)' select 'Read from file' and browse for the file
which contains the data you want to get.

To check which file contains which data check the .ini file which specifies it in their respective
category that match up with the name of the folders.

# Suggestions and Reporting Errors

If the program crashed or you have a suggestions check in with https://bitbucket.org/GreenyNeko/obsgw2filesystem/issues?status=new&status=open 
and create a new issue there. If it's an error make sure to upload the error.log. Additionally make sure you set the api key to hidden as the file
may be publicly available.

# Credit

Credit is not needed as long as you do not claim the program to be your own. That said.. I would really like to be given credit for the work or tips.

Also check out my other content:

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Patreon: https://www.patreon.com/GreenyNeko
Twitter: https://twitter.com/GreenNekoHaunt
DevianArt: https://greennekohaunt.deviantart.com/
Twitch: https://www.twitch.tv/greenyneko
Calendar: https://calendar.google.com/calendar/embed?src=8j4jaj4gusq5k5jp3uao6ac9io%40group.calendar.google.com
Blog: https://www.greenyneko.com/
SoundCloud: https://soundcloud.com/pascal-neubert-greennekohaunt
